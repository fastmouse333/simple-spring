package springtest.simplespring.model;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "b")
public class B
{
  @Id
  @GeneratedValue(strategy = IDENTITY)
  @Column(name = "id", unique = true, nullable = false)
  private Integer id;

  @Column(name = "display_name")
  private String displayName;
  
  @ManyToOne
  @JoinColumn(name = "a_id", nullable = false)
  private A a;

  public B() {
  }

  public B(Integer id, String displayName, A a) {
    this.id = id;
    this.displayName = displayName;
    this.a = a;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getDisplayName() {
    return displayName;
  }

  public void setDisplayName(String displayName) {
    this.displayName = displayName;
  }

  public A getA() {
    return a;
  }

  public void setA(A a) {
    this.a = a;
  }
}
