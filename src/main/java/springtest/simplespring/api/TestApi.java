package springtest.simplespring.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import springtest.simplespring.service.ThreadParent;

@RestController
public class TestApi
{
  @Autowired
  private ThreadParent test2;
  /**
   * Get Groups info
   * @return
   */
  @GetMapping(value="${test.url}")
  public ResponseEntity<String> testApi()
  {
    test2.mainTest();
    return ResponseEntity.ok("Test Async OK");
  }
  
}
