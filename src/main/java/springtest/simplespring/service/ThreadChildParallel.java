package springtest.simplespring.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class ThreadChildParallel
{
  @Async
  public void printString()
  {
    List<Integer> lists = new ArrayList<>();
    for (int i=0; i < 5; i++)
    {
      lists.add(i);
    }
    lists.parallelStream().forEach(e -> runToTest(e));
    System.out.println("This is thread with parallel");
  }
  
  private void runToTest(int i)
  {
    try
    {
      Thread.sleep(2000 + i);
    }
    catch (InterruptedException e)
    {
      e.printStackTrace();
    }
  }
}
