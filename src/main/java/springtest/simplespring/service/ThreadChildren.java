package springtest.simplespring.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class ThreadChildren
{
  @Async
  public void printString()
  {
    List<Integer> lists = new ArrayList<>();
    for (int i=0; i < 10; i++)
    {
      lists.add(i);
    }
    for(Integer element: lists)
    {
      runToTest(element);
    }
    System.out.println("This is simple thread");
  }
  
  private void runToTest(int i)
  {
    try
    {
      Thread.sleep(2000 + i);
    }
    catch (InterruptedException e)
    {
      e.printStackTrace();
    }
  }
  
}