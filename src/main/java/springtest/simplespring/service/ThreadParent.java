package springtest.simplespring.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ThreadParent
{
  @Autowired
  private ThreadChildren threadChild;
  
  @Autowired
  private ThreadChildParallel threadChildParallel;
  
  public void mainTest()
  {
    System.out.println("Start parent thread");
    threadChild.printString();
    threadChildParallel.printString();
    System.out.println("End parent thread");
  }
  
}
